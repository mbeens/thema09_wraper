package nl.bioinf.mbeens.machineLearning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Attribute;

/**
 * A class that contains the data of a patient.
 */
public class Patient {

    /** The name of the "name" attribute. */
    private final String attributeName = "ID";

    /** The name of the patient */
    private final String name;

    /** A collection of antibodies with there count data. */
    private final Map<String, Integer> antibodies = new HashMap<>();

    /** A list of patient names used to create the name Atribute in the toInstance function. */
    private ArrayList<String> allPatientNames = new ArrayList<>();

    /**
     * The constructor of this class
     * @param name The name of the patient
     */
    public Patient(String name){
        this.name = name;
        this.allPatientNames.add(name);
    }

    /**
     * This function gives the name of the patient
     * @return The name of the patient
     */
    public String getName() {
        return name;
    }

    /**
     * This function gives the antibodies and there counts of the patient
     * @return A list containing antibodies and counts of the patients.
     */
    public Map<String, Integer> getAntibodies() {
        return antibodies;
    }

    /**
     * This function gives a list of attributes that the patient has.
     * @return A list of attributes that this patient has.
     */
    public ArrayList<Attribute> getAttributes(){
        ArrayList<Attribute> attributes = new ArrayList<>();
        Attribute name = new Attribute(attributeName , allPatientNames, 0);
        attributes.add(name);
        for (int i = 0; i<this.antibodies.keySet().size(); i++) {
            String key = (String) this.antibodies.keySet().stream().sorted().toArray()[i];
            Attribute attr = new Attribute(key, i+1);
            attributes.add(attr);
        }

        return attributes;
    }

    /**
     * This function sets the antibody count for each of the antibodies measured.
     * @param header A string array that contains all the names of the antibodies
     * @param counts A string array that contains all the counts of the antibodies
     */
    public void setAntibodies(String[] header, String[] counts) {
        for (int i=1;  i<header.length; i++){
            antibodies.put(header[i], Integer.parseInt(counts[i]));
        }
    }

    /**
     * With this class you can set the all names of the patients in the dataset
     * @param allPatientNames A list of names from the patients.
     */
    public void setAllPatientNames(ArrayList<String> allPatientNames){
        this.allPatientNames = allPatientNames;
    }

    /**
     * A function that returns a string representation of the class.
     * @return A string representation of the class
     */
    public String toString(){
        return this.name + ": " + this.antibodies + "\n";
    }

    /**
     * A function that converts the patient class to an instance class for Weka.
     * @return A instance that contains the name and antibody data.
     */
    public Instance toInstance(){
        Instance inst = new DenseInstance(this.antibodies.size()+1);

        for (Attribute attribute: getAttributes()) {
            if (attribute.name().equals(attributeName)){
                inst.setValue(attribute, this.name);
            }else {
                inst.setValue(attribute, this.antibodies.get(attribute.name()));
            }
        }
        return inst;
    }
}
