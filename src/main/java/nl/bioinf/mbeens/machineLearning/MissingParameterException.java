package nl.bioinf.mbeens.machineLearning;

public class MissingParameterException extends Exception{
    public MissingParameterException(String message){
        super(message);
    }
}
