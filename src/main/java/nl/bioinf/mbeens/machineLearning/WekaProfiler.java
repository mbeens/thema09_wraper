package nl.bioinf.mbeens.machineLearning;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.util.ArrayList;

/**
This class will classify a patient using the weka model.
 */
public class WekaProfiler {

    /** A list of attributes that are needed for classification. */
    private ArrayList<Attribute> attributes;

    /**
     * Read in a weka Model and read a CSV file
     * @param args The command line arguments given;
     */
    public static void main(String[] args) {
        WekaProfiler profiler = new WekaProfiler();
        profiler.classify(args[0]);
    }

    /**
     * Classifys all patients in a CSV string.
     * @param file An CSV file that contains all the patient data
     */
    public void classify(String file){
        // Read the given CSV
        Patient[] patients = readCSV(file);

        // Process Patients
        ArrayList<Patient> patientsProcessed = processPatients(patients);

        // Convert the patients list to Instances
        Instances patientInstances = convertPatientsToInstances(patientsProcessed);

        // Add the Class to the Instances
        patientInstances = addClassToInstances(patientInstances);

        // Remove IDs for classification
        Instances tmpPatientInstances = removeIdForClassification(patientInstances);

        // Classify patients
        Instances patientsClasified = classifyPatients(tmpPatientInstances);

        // Add the IDs back
        addIdBack(patientsClasified, patientInstances, patientsProcessed);

        // Export the classified patients to a csv
        exportClasifiedToCSV(patientsClasified);
        //System.out.println(patientsClasified);
    }

    /**
     * Reads a csv file Using the ReaderCSV class.
     * @param file The filepath/name of the csv file
     * @return A list of patients.
     */
    private Patient[] readCSV(String file){
        Patient[] patients = null;
        try {
            ReaderCSV patientCSV = new ReaderCSV(file);
            patients = patientCSV.getPatients();
        }catch (Exception e){
            e.printStackTrace();
        }
        return patients;
    }

    /**
     * Process a list of patients using the ProcessPatient Class.
     * @param patients A array of patients that need to be processed.
     * @return A list of patients that need to be processed.
     */
    private ArrayList<Patient> processPatients(Patient[] patients){
        ArrayList<Patient> patientsProcessed = new ArrayList<>();
        for (Patient patient: patients){
            patient.setAllPatientNames(getPatientNames(patients));
            ProcessPatient process = new ProcessPatient(patient);
            patientsProcessed.add(process.getPatient());
        }
        return patientsProcessed;
    }

    /**
     * Convert the patient array to an Instances Class using the toInstance function from the patient class
     * @param patientsProcessed A Patient list that needs to be converted
     * @return The converted patients as Instances
     */
    private Instances convertPatientsToInstances(ArrayList<Patient> patientsProcessed) {
        // Convert the patients list to Instances
        attributes = patientsProcessed.get(0).getAttributes();
        Instances patientInstances = new Instances("unknownPatients", attributes, patientsProcessed.size());
        for (Patient patient : patientsProcessed) {
            Instance patientInstance = patient.toInstance();
            patientInstances.add(patientInstance);
        }
        return patientInstances;
    }

    /**
     * Add the class instances to the Instances
     * @param patientInstances The instances of the patients
     * @return The same instances with a extra attribute labeled as the class
     */
    private Instances addClassToInstances(Instances patientInstances){
        try {
            patientInstances = addAttribute(patientInstances, "diseaseDuration", "last", "< 1,2 - 6,7 - 11,12 - 16,17 - 21,22 - 26,27 >");
            attributes.add(patientInstances.attribute("diseaseDuration"));
        }catch (Exception e){
            e.printStackTrace();
        }
        // Set the class
        patientInstances.setClassIndex(patientInstances.numAttributes() - 1);

        return patientInstances;
    }

    /**
     * Removes the First column of the Instances as the is class is not used in classification.
     * @param patientInstances The patient instances that need to be classified.
     * @return A copy of the patientInctances with the first column removed.
     */
    private Instances removeIdForClassification(Instances patientInstances){
        Instances tmpPatientInstances = null;
        try {
            tmpPatientInstances = removeAttribute(patientInstances, GetNumberArray(1, patientInstances.numAttributes()));
            attributes.remove(0);
        }catch (Exception e){
            e.printStackTrace();
        }
        return tmpPatientInstances;
    }

    /**
     * Classifies each patient with the weka model using the WekaModel class.
     * @param patientInstances The instances of patients that need to be classified
     * @return Instances of patients that are classified.
     */
    private Instances classifyPatients(Instances patientInstances){
        // Load classifier
        WekaModel classifier = new WekaModel("data/models/SimpleLogistic2.model");

        // Classify patients
        Instances patientsClassified = new Instances("ClassifiedPatients", attributes, patientInstances.size());
        for (Instance patient: patientInstances){
            try {
                patientsClassified.add(classifier.classifyPatient(patient));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return patientsClassified;
    }

    /**
     * Add the fist column back based on the preclassified patient instances.
     * @param classified The Instances of the classified patients
     * @param patientInstances The instances of the non classified patients
     * @param patientsProcessed A list of patients containing the patient data.
     */
    private void addIdBack(Instances classified, Instances patientInstances, ArrayList<Patient> patientsProcessed){
        classified.insertAttributeAt(patientInstances.attribute(0), 0);

        for (int i = 0; i<classified.size(); i++){
            String name = patientsProcessed.get(i).getName();
            classified.instance(i).setValue(0, name);
        }
    }

    private void exportClasifiedToCSV(Instances ClassifiedPatients){
        CSVSaver saver = new CSVSaver();
        saver.setInstances(ClassifiedPatients);
        try{
            saver.setFile(new File("data/patientsClasified.csv"));
            saver.writeBatch();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Adds an attribute to a Instances object
     * @param data The instances where an attribute needs to be added.
     * @param name The name of the attribute
     * @param index The position of where the attribute needs to be added
     * @param labels The labels of the attribute.
     * @return The instances with an attribute added
     * @throws Exception When the attribute cant be added
     */
    private static Instances addAttribute(Instances data, String name, String index, String labels) throws Exception {
        Add classification = new Add();
        classification.setAttributeIndex(index);
        classification.setNominalLabels(labels);
        classification.setAttributeName(name);
        classification.setInputFormat(data);
        return Filter.useFilter(data, classification);
    }

    /**
     * Removes an attribute from an instances object
     * @param data The instances were a attribute needs to be removed
     * @param itemsToKeep The columns that need te stay.
     * @return The instances were a attribute is removed.
     * @throws Exception When an attribute cant be removed.
     */
    private static Instances removeAttribute(Instances data, int[] itemsToKeep) throws Exception {

        Remove removeFilter = new Remove();
        removeFilter.setAttributeIndicesArray(itemsToKeep);
        removeFilter.setInvertSelection(true);
        removeFilter.setInputFormat(data);
        return Filter.useFilter(data, removeFilter);
    }

    /**
     * Gives an range of integers from the start value to the end value
     * @param start The start value of the range. Needs to be higher than the stop
     * @param stop The end value of the range.
     * @return A integer array of each integer value between the start and stop value
     */
    public static int[] GetNumberArray(int start, int stop){
        int[] newArr = new int[stop-start];
        for (int i = start; i<stop; i++){
            int newIndex = i-start;
            newArr[newIndex] = i;
        }
        return newArr;
    }

    /**
     * Creates a list of all the patient names
     * @param patients a array of patients.
     * @return A String list of patient names
     */
    private static ArrayList<String> getPatientNames(Patient[] patients){
        ArrayList<String> names = new ArrayList<>();
        for (Patient patient: patients){
            names.add(patient.getName());
        }
        return names;
    }

}