package nl.bioinf.mbeens.machineLearning;

import java.util.ArrayList;
import java.util.Map;

/**
 * This class needs to given a set of patients remove the unwanted antibodies.
 */
public class ProcessPatient {

    /** The patient being processed. */
    private final Patient patient;

    /** All the parameters needed for successful classification.  Make confic variable*/
    private String[] modelParams;

    /**
     * The constructor for this class and process the patient.
     * @param patient The patient that needs to be processed.
     */
    public ProcessPatient(Patient patient){
        this.patient = patient;

        // Get modelParams from config
        try {
            this.modelParams = new ConfigReader().getModelParams();
        }catch (Exception e){
            e.printStackTrace();
        }

        // Isolate the model Params
        IsolateModelParams();
    }

    /**
     * Gives the patient that has been processed
     * @return The processed patient.
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * Isolates the antibodies needed to classify the patient. And removes the rest of the antibodies
     */
    private void IsolateModelParams(){
        Map<String, Integer> x = patient.getAntibodies();
        ArrayList<String> counts = new ArrayList<>();
        counts.add("");
        for (int i = 1; i<modelParams.length; i++){
            counts.add(x.get(modelParams[i]).toString());
        }
        patient.setAntibodies(modelParams, GetStringArray(counts));
    }

    /**
     * Function to convert ArrayList<String> to String[]
     * @param arr A String ArrayList
     * @return the converted String Array
     */
    public static String[] GetStringArray(ArrayList<String> arr)
    {
        // declaration and initialise String Array
        String[] str = new String[arr.size()];

        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {

            // Assign each value to String array
            str[j] = arr.get(j);
        }
        return str;
    }
}
