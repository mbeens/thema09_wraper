package nl.bioinf.mbeens.machineLearning;
import weka.classifiers.AbstractClassifier;
import weka.core.Instance;

import java.io.File;

public class WekaModel {

    /** The file path and file name of the weka model. */
    private final String modelFile;

    /** The classifier model*/
    private AbstractClassifier model;

    /**
     * The constructor of the weka model that loads the classifier.
     * @param file The file path and file name of the weka model.
     */
    public WekaModel(String file){
        this.modelFile = file;
        try {
            loadClassifier();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Loads the classifier model.
     * @throws Exception throws a exception if the file can not be read.
     */
    private void loadClassifier() throws Exception {
        // deserialize model
        File file = new File(modelFile);
        this.model = (AbstractClassifier) weka.core.SerializationHelper.read(file.getAbsolutePath());
    }

    /**
     * Classifies a patient and returns the classified patient.
     * @param patient An instance that needs to be classified.
     * @return A classified instance
     * @throws Exception throws a exception if the classification fails.
     */
    public Instance classifyPatient(Instance patient) throws Exception {
        // create copy
        Instance labeled = (Instance) patient.copy();

        // label instances
        double clsLabel = model.classifyInstance(patient);
        labeled.setClassValue(clsLabel);

        // Add name
        return labeled;
    }

}
