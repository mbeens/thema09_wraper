package nl.bioinf.mbeens.machineLearning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * This class reads a CSV file and converts each line to a Patient class.
 * - The CSV needs a "," as separator.
 */
public class ReaderCSV {

    /** The header names of the CSV file. */
    private String[] header;

    /** All the parameters needed for successful classification.  Make confic variable*/
    private String[] modelParams;

    /** A list containing the read patient. */
    private final ArrayList<Patient> patients = new ArrayList<>();

    /**
     * This function reads the CSV file and converts each line in a Patient class
     * @param filename The name and filepath of the CSV file.
     * @throws MissingParameterException Throws a missing parameter exemption if there is a parameter missing.
     */
    public ReaderCSV(String filename) throws MissingParameterException{
        // Open the csv files
        try {
            boolean firstLine = true;

            File file = new File(filename);
            Scanner file_data = new Scanner(file);
            while (file_data.hasNextLine()) {
                String data = file_data.nextLine();
                data = data.replace("\"", "");
                if (firstLine){
                    header = data.split(",");
                    String message = CheckValidCSV();
                    if (message.length()>0){
                        throw new MissingParameterException(message);
                    }
                    firstLine = false;
                }else{
                    patients.add(processPatient(data.split(",")));
                }
            }
            file_data.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * This function checks if the given csv has all the required parameters using the variable neededParams
     * @return An error message indication the missing parameter
     */
    private String CheckValidCSV(){
        // Load modelParams from config
        try {
            this.modelParams = new ConfigReader().getModelParams();
        }catch (Exception e){
            e.printStackTrace();
        }

        for (String param: modelParams){
            if (!Arrays.stream(header).collect(Collectors.toList()).contains(param)){
                return "A parameter is missing, needed: " + param;
            }
        }
        return "";
    }

    /**
     * Processes a line into a Patient class
     * @param patientData A String list containing each word in a line.
     * @return An Patient class containing the data of the line
     */
    private Patient processPatient(String[] patientData){
        Patient patient = new Patient(patientData[0]);
        patient.setAntibodies(header, patientData);
        return patient;
    }

    /**
     * Gives a array of the patients read from the CSV file.
     * @return An Patient[] containing the data form the CSV.
     */
    public Patient[] getPatients() {
        return patients.toArray(new Patient[0]);
    }
}
