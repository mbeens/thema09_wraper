import nl.bioinf.mbeens.machineLearning.Patient;
import nl.bioinf.mbeens.machineLearning.ReaderCSV;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReaderCSVTest {

    @Test
    void getPatientsTest() {
        try {
            // Create patient equal to the getPatientTest csv.
            String[] header = new String[]{
                    "DONOR", "Ro52_EL_SC","Ku_EL_SC","PM75_EL_SC","PM100_EL_SC","ThTo_EL_SC","Fib_EL_SC",
                    "RP.155_EL_SC","RP11_EL_SC","CENPB_EL_SC","CENPA_EL_SC","Scl.70_EL_SC",
                    "Hist_EL_ANA","NUC_EL_ANA","dsDNA_EL_ANA","PCNA_EL_ANA","SSA_EL_ANA",
                    "Sm_EL_ANA","RNP.Sm_EL_ANA","PGDH_EL_ED","SL.ALP_EL_ED","LC.1_EL_ED",
                    "LKM.1_EL_ED","PML_EL_ED","M2.3E_EL_ED","CENPA_DTEK_SSC"};
            String[] counts = new String[]{
                    "A101","39","1","95","2","1","1","2","1","1","4","3","61","30","8","1","0","0",
                    "61","7","2","1","1","3","3","13"};
            Patient patient = new Patient("A101");
            patient.setAntibodies(header, counts);

            Patient[] patients = new Patient[]{patient};
            // Read patient
            ReaderCSV test = new ReaderCSV("data/test/getPatiensTest.csv");

            for (int i = 0; i<patients.length; i++){
                assertEquals(patients[i].toString(), test.getPatients()[i].toString());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
