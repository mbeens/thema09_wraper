# README #

# Java wrapper Classifier
This java wraper classifies how long a patient has been sick for.

## Usage
I had a problem when I tried to create the jar file because the Build artifacts did not do anything. So you have to create the
jar yourself with intelij or another program you use. When created you can execute the jar via the method below

```
java thema09.jar <patients file>
```
patient file is an CSV that contains all the patiens that need to be classified.

## Output
This wrapper outputs a CSV with all the original data and an extra column with the predicted disease duration.

Created by: Micha Snippe
Version: 1.0.0